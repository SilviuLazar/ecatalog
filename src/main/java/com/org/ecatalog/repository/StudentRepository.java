package com.org.ecatalog.repository;

import com.org.ecatalog.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface StudentRepository extends JpaRepository<Student, UUID> {

    @Override
    Student save(Student entity);

   List<Student> getStudentBySchoolClassId(UUID schoolClassId);

}
