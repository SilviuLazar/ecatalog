package com.org.ecatalog.service;

import com.org.ecatalog.dto.TeacherCardDto;
import com.org.ecatalog.dto.TeacherCreateDto;
import com.org.ecatalog.dto.TeacherDto;
import com.org.ecatalog.entity.Teacher;
import com.org.ecatalog.mapper.TeacherMapper;
import com.org.ecatalog.repository.TeacherRepository;
import jakarta.persistence.EntityManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@AllArgsConstructor
@Component
public class TeacherService {
    private final TeacherRepository teacherRepository;

    private final TeacherMapper teacherMapper;

    private final EntityManager entityManager;

    public TeacherDto create(TeacherCreateDto createDto){
        Teacher toBeSaved = teacherMapper.toEntity(createDto);
        Teacher saved = teacherRepository.save(toBeSaved);
        return teacherMapper.toDto(saved);
    }

    public List<TeacherCardDto> getTeachersCards() {
    return teacherMapper.toTeacherCardDtoList(teacherRepository.findAll());
    }
}
