package com.org.ecatalog.service;

import com.org.ecatalog.dto.StudentCardDto;
import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;
import com.org.ecatalog.dto.TeacherCardDto;
import com.org.ecatalog.entity.Student;
import com.org.ecatalog.mapper.StudentMapper;
import com.org.ecatalog.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;


    public StudentDto create(StudentCreateDto createDto) {
        Student toBeSaved = studentMapper.toEntity(createDto);
        Student saved = studentRepository.save(toBeSaved);
        return studentMapper.toDto(saved);

    }

    public List<StudentDto> getAll() {
        List<Student> students = studentRepository.findAll();
        return studentMapper.toDtoList(students);
    }

    public List<StudentDto> getAllStudentsFromAClass(UUID schoolClassId) {
        List<Student> studentsList = studentRepository.getStudentBySchoolClassId(schoolClassId);
        return studentMapper.toDtoList(studentsList);

    }
    public List<StudentCardDto> getStudentCards() {
        return studentMapper.toStudentCardDtoList(studentRepository.findAll());
}
}
