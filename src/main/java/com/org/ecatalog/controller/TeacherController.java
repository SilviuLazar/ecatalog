package com.org.ecatalog.controller;

import com.org.ecatalog.dto.TeacherCardDto;
import com.org.ecatalog.dto.TeacherCreateDto;
import com.org.ecatalog.dto.TeacherDto;
import com.org.ecatalog.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teachers")
@AllArgsConstructor
public class TeacherController {
    private final TeacherService teacherService;
    @PostMapping
    public TeacherDto create(@RequestBody TeacherCreateDto teacherCreateDto){
        return teacherService.create(teacherCreateDto);
    }
    @GetMapping(path = "/teachers_cards")
    public List<TeacherCardDto> getTeachersCards(){
        return teacherService.getTeachersCards();
    }

}
